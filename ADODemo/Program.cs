﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace ADODemo // ADO = ActiveX Data Objects
{
    struct Asi
    {
        public int Id;
        public string Nimetus;
        public decimal Hind;
        public override string ToString()
        {
            return $"{Id}: {Nimetus} maksab {Hind}";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Asi> asjad = new List<Asi>();

            string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\sarvi\source\repos\ADODemo\ADODemo\Properties\Asjad.mdf;Integrated Security=True;Connect Timeout=30";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();



                #region Datareader
                SqlCommand comm = new SqlCommand("Select * from asjad", conn);
                using (SqlDataReader R = comm.ExecuteReader())
                {

                    while (R.Read())
                    {
                        asjad.Add(new Asi()
                        {
                            Id = R.GetFieldValue<int>(0),
                            Nimetus = R.GetFieldValue<string>(1).Trim(),
                            Hind = R.GetFieldValue<decimal>(2)
                        });
                    }

                }
                #endregion
            

            //            foreach(var x in asjad) Console.WriteLine(x);

                SqlDataAdapter adapt = new SqlDataAdapter("Select * from asjad", conn);
                DataSet data = new DataSet();
                adapt.TableMappings.Add("Table", "Asjad");
                adapt.Fill(data);

                DataTable asjadeTabel = data.Tables["Asjad"];
                foreach(var x in asjadeTabel.AsEnumerable())
                {
 //                   Console.WriteLine(x["Nimetus"]);
                }

                //data.WriteXml(@"..\..\asjad.xml");

                AsjadDS asjadDS = new AsjadDS();
                AsjadDSTableAdapters.AsjadTableAdapter asjadAdapter = new AsjadDSTableAdapters.AsjadTableAdapter();
                //asjadAdapter.Fill(asjadDS.Asjad);

                //foreach (var x in asjadDS.Asjad.AsEnumerable())
                  //  Console.WriteLine($"{x.Id}: {x.Nimetus} maksab {x.Hind}");


                NWDataset nwData = new NWDataset();
                NWDatasetTableAdapters.CategoriesTableAdapter catAdapter = new NWDatasetTableAdapters.CategoriesTableAdapter();
                NWDatasetTableAdapters.ProductsTableAdapter prodAdapter = new NWDatasetTableAdapters.ProductsTableAdapter();
                catAdapter.Fill(nwData.Categories);
                prodAdapter.Fill(nwData.Products);

                var q = from p in nwData.Products.AsEnumerable()
                        where !p.Discontinued
                        orderby p.UnitPrice
                        select new { p.ProductName, p.UnitPrice };
                foreach (var x in q) Console.WriteLine(x);

                var q2 = from p in nwData.Products.AsEnumerable()
                         join c in nwData.Categories.AsEnumerable()
                         on p.CategoryID equals c.CategoryID
                         orderby c.CategoryName, p.ProductName
                         descending
                         select new { c.CategoryName, p.ProductName, p.UnitPrice }
                         ;

                foreach (var x in q2) Console.WriteLine(x);


                //var q3 = from p in nwData.Products.AsEnumerable()
                //         group p by p.CategoryID into g






            }

        }
    }
}
